module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    transform: {
      '^.+\\.ts?$': 'ts-jest',
    },
    collectCoverage: true,
    collectCoverageFrom: [
      '<rootDir>/src/**/*.ts',
      '!<rootDir>/src/**/*.interface.ts',
      '!<rootDir>/src/**/*.mock.ts',
      '!<rootDir>/src/**/*.module.ts',
      '!<rootDir>/src/**/*.spec.ts',
      '!<rootDir>/src/**/*.test.ts',
      '!<rootDir>/src/**/*.d.ts'
  ],
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
  };
  