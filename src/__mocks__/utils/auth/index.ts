export const authResponseServiceMock = {
  id_token:
    'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2Y3J1dmluZWxyQGdtYWlsLmNvbSIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE2NTk2OTg3NjJ9.1N5pp_l-8y-B5Bp7MbUEbGR7Bwc7Yxdw5zwzsjdx8HXbXQBf7b0lLCr_6hUgDRPsSC0m8-IZbXBY4_bW7xL7gw',
};

export const leafauthResponse = {
  idToken:
    'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2Y3J1dmluZWxyQGdtYWlsLmNvbSIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE2NTk2OTg3NjJ9.1N5pp_l-8y-B5Bp7MbUEbGR7Bwc7Yxdw5zwzsjdx8HXbXQBf7b0lLCr_6hUgDRPsSC0m8-IZbXBY4_bW7xL7gw',
  authorization:
    'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2Y3J1dmluZWxyQGdtYWlsLmNvbSIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE2NTk2OTg3NjJ9.1N5pp_l-8y-B5Bp7MbUEbGR7Bwc7Yxdw5zwzsjdx8HXbXQBf7b0lLCr_6hUgDRPsSC0m8-IZbXBY4_bW7xL7gw',
};

export const leafAuthResponseError = {
  error: {
    message: "\"password\" is not allowed to be empty"
  }
}
