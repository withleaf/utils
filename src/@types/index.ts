export interface AuthAPIProps {
  username: string;
  password: string;
  rememberMe: boolean;
}
