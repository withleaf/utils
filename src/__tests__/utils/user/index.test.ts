import MockAdapter from 'axios-mock-adapter';
import { leafUserApi } from '../../../api/leaUserApi';
import { getAllLeafUsers } from '../../../utils/user';

describe('Test User APIs', () => {
  it('should get all leaf users with Bearer Token', async () => {
    const mock = new MockAdapter(leafUserApi);
    const bearerToken = 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2Y3J1dmluZWxyQGdtYWlsLmNvbSIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE2NTk2OTg3NjJ9.1N5pp_l-8y-B5Bp7MbUEbGR7Bwc7Yxdw5zwzsjdx8HXbXQBf7b0lLCr_6hUgDRPsSC0m8-IZbXBY4_bW7xL7gw'

    const responseData = {
      data: undefined,
      status: undefined,
      statusMessage: undefined
    }
    mock.onPost('/users').replyOnce(200, responseData);
    const response = await getAllLeafUsers(bearerToken);
    expect(response).toEqual(responseData);
  });

  it('should get all leaf users with token', async () => {
    const mock = new MockAdapter(leafUserApi);
    const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2Y3J1dmluZWxyQGdtYWlsLmNvbSIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE2NTk2OTg3NjJ9.1N5pp_l-8y-B5Bp7MbUEbGR7Bwc7Yxdw5zwzsjdx8HXbXQBf7b0lLCr_6hUgDRPsSC0m8-IZbXBY4_bW7xL7gw'

    const responseData = {
      data: undefined,
      status: undefined,
      statusMessage: undefined
    }
    mock.onPost('/users').replyOnce(200, responseData);
    const response = await getAllLeafUsers(token);
    expect(response).toEqual(responseData);
  });

  it('should thrown an error', async () => {
    const mock = new MockAdapter(leafUserApi);

    const responseData = {
      data: undefined,
      status: undefined,
      statusMessage: undefined
    }
    mock.onPost('/users').replyOnce(500)
    const response = {
      error: 'unexpected error'
    };
    expect(response).toEqual({ error: 'unexpected error' });
    expect(response).toHaveProperty('error');
    expect(response).toBeInstanceOf(Object);
  });

});
