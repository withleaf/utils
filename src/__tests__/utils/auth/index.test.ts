import MockAdapter from 'axios-mock-adapter';
import { leafAuthApi } from '../../../api/leafAuthApi';
import { authResponseServiceMock, leafauthResponse, leafAuthResponseError } from '../../../__mocks__/utils/auth';
import { LeafAuth } from '../../../';

describe('get leaf token', () => {
  it('should request and return a token', async () => {
    const mock = new MockAdapter(leafAuthApi);
    const request = {
      username: 'test@withleaf.io',
      password: '123456',
      rememberMe: true,
    };

    mock.onPost('/authenticate').replyOnce(200, authResponseServiceMock);
    const response = await LeafAuth.token(request);
    expect(response).toEqual(leafauthResponse);
  });

  it('should request a token and return an error message from an empty password', async () => {
    const mock = new MockAdapter(leafAuthApi);
    const request = {
      username: 'test@withleaf.io',
      password: '',
      rememberMe: true,
    };

    mock.onPost('/authenticate').replyOnce(200, authResponseServiceMock);
    const response = await LeafAuth.token(request);
    expect(response).toEqual(leafAuthResponseError);
  });

});
