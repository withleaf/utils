import { interceptUserApi, leafUserApi } from '../../api/leaUserApi';

const getAllLeafUsers = async (token: string) => {
  try {
    interceptUserApi(token);
    const response = await leafUserApi.get('/users');
    return response.data;
  } catch (error: any) {
    return {
      status: error.status,
      statusMessage: error.statusMessage,
      data: error.data,
    };
  }
};

const getLeafUserById = async (token: string, userId: string) => {
  try {
    interceptUserApi(token);
    const response = await leafUserApi.get(`/users/${userId}`);
    return response.data;
  } catch (error: any) {
    return error;
  }
};

export { getAllLeafUsers, getLeafUserById };
