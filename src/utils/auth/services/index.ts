import { AuthAPIProps } from '../../../@types';
import { leafAuthApi } from '../../../api/leafAuthApi';

const leafTokenService = async (userData: AuthAPIProps) : Promise<any> => {
  const requestData: any = { username: userData.username, password: userData.password, rememberMe: userData.rememberMe };
  const { data } = await leafAuthApi.post('/authenticate', requestData);
  return data
};

export { leafTokenService };
