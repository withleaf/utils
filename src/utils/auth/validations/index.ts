import Joi from 'joi';
import { AuthAPIProps } from '../../../@types';

const validateTokenRequest = (userData: AuthAPIProps) => {
  const requestTokenSchema = Joi.object({
    username: Joi.string().email({ tlds: { allow: false } }).required(),
    password: Joi.string().required(),
    rememberMe: Joi.boolean().required(),
  })

  const { error } = requestTokenSchema.validate(userData, { abortEarly: false });

  if (error) {
    return error.message;
  } else {
    return true;
  }

}

export { validateTokenRequest }