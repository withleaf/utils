import { AuthAPIProps } from "../../../@types";
import { leafTokenService } from "../services"
import { validateTokenRequest } from "../validations";

const leafTokenController = async (userData: AuthAPIProps): Promise<any> => {
    const validation = validateTokenRequest(userData);

    if (typeof validation === 'boolean') {
        try {
            const token = await leafTokenService(userData);
            return {
                idToken: token.id_token,
                authorization: `Bearer ${token.id_token}`,
            }
        } catch (error) {
            return {
                error: {
                    message: error.message,
                    detail: error.response.data.detail,
                    code: error.response.status

                }
            }
        }
    } else {
        return {
            error: {
                message: validation
            }
        }
    }

};

export {
    leafTokenController
}