import axios, { AxiosRequestConfig } from 'axios';

const leafUserApi = axios.create();

leafUserApi.interceptors.request.use((config) => {
  config.baseURL = `https://api.withleaf.io/services/usermanagement/api`;
  config.responseType = 'json';
  return config;
});

const interceptUserApi = (token: string) => {
  return leafUserApi.interceptors.request.use(
    (config: AxiosRequestConfig) => {
      token.split(' ')[0] === 'Bearer'
        ? (config.headers!.Authorization = token)
        : (config.headers!.Authorization = `Bearer ${token}`);
      return config;
    },
    (error) => Promise.reject(error),
  );
};

export { leafUserApi, interceptUserApi };
