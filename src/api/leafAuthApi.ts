import axios from 'axios';

const leafAuthApi = axios.create();

leafAuthApi.interceptors.request.use((config) => {
  config.baseURL = `https://api.withleaf.io/api`;
  config.responseType = 'json';
  return config;
});

export { leafAuthApi };
